
import 'react-native-gesture-handler';
import * as React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

import aboutPage from './src/pages/aboutPage'
import detailPage from './src/pages/detailPage'
import gejalaPage from './src/pages/gejalaPage'
import loginPage from './src/pages/loginPage'
import mainPage from './src/pages/mainPage'
import splash from './src/pages/splash'
const App = () => {
  return (

    <NavigationContainer>
      <Stack.Navigator initialRouteName={splash}>
        <Stack.Screen name="Splash" component={splash} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={loginPage} options={{ headerShown: false }} />
        <Stack.Screen name="Main" component={mainPage} options={{ headerShown: false }} />
        <Stack.Screen name="Detail" component={detailPage} options={{ headerShown: false }} />
        <Stack.Screen name="Gejala" component={gejalaPage} options={{ headerShown: false }} />
        <Stack.Screen name="About" component={aboutPage} />
      </Stack.Navigator>
    </NavigationContainer>

  );
};

export default App;
