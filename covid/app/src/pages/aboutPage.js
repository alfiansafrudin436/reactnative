import React from 'react'
import {
    StyleSheet, View, Text, TextInput, Button, TouchableOpacity, Image, ScrollView, SafeAreaView
} from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import { useFonts } from 'expo-font';

const aboutPage = (navigation) => {
    useFonts({
        'Cherry Swash': require('../fonts/CherrySwash-Bold.otf'),
    });
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Cherry Swash',
                    fontSize: 80,
                    top: 0,
                    left: "35%",
                    right: "35%",
                    marginTop: 10,
                    fontWeight: '700',
                    color: '#7B61FF',
                }}>CO</Text>
                <Text style={{
                    position: 'absolute',
                    fontSize: 80,
                    top: 70,
                    left: "32.5%",
                    right: "32.5%",
                    marginTop: 10,
                    fontWeight: '700',
                    color: '#7B61FF',
                }}>-19</Text>
                <AntDesign name="user" size={50} color="#BDBDBD" style={{
                    top: 230,
                    left: 170,
                    borderColor: '#BDBDBD', position: 'absolute'
                }} />
                <Text style={{
                    position: 'absolute',
                    fontSize: 18,
                    left: 140,
                    fontWeight: '600',
                    color: '#BDBDBD',
                    top: 300
                }}>Alfian Safrudin</Text>
            </View >
            <View style={styles.body}>
                <View style={{
                    position: 'absolute',
                    backgroundColor: '#7B61FF',
                    height: 360,
                    width: "100%",
                    top: -120,
                    borderTopLeftRadius: 40,
                    borderTopRightRadius: 40
                }}>
                </View>
                <AntDesign name="google" size={30} color="white" style={{ position: 'absolute', left: 180, top: -40 }} />
                <Text style={{
                    position: 'absolute',
                    fontSize: 18,
                    left: 80,
                    fontWeight: '600',
                    color: 'white',
                    top: -10
                }}>alfiansafrudin436@gmail.com</Text>
            </View>

        </View >
    )
}

export default aboutPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        width: 400,
        height: 800
    },
    header: {
        flex: 1,
        backgroundColor: 'white',
        marginBottom: 10,
        borderBottomLeftRadius: 130,
        textAlign: 'center',
        paddingTop: "80%",
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        // marginBottom: 10,
        textAlign: 'center',
        paddingTop: "20%",
    },

})
