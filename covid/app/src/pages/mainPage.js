import React, { useState, useEffect } from 'react'
import {
    StyleSheet, View, Text, TextInput, Button, TouchableOpacity, Image, ScrollView, SafeAreaView
    , FlatList
} from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import { ListItem } from 'react-native-elements'
import axios from 'axios';
import { LineChart, Path, Grid } from 'react-native-svg-charts'


const mainPage = ({ navigation }) => {
    const [data, setData] = useState([]);

    useEffect(() => {
        axios
            .get("https://api.covid19api.com/country/indonesia")
            .then(result => setData(result.data));
    }, []);

    {
        var Country = data.map(item => (
            <Text key={item.Country}>
                {(item.Country)}
            </Text>
        ))
        var datas = data.map(item => (
            <Text key={item.Confirmed}>
                {(item.Confirmed)}
            </Text>
        ))
        var Recovered = data.map(item => (
            <Text key={item.Recovered}>
                {(item.Recovered)}
            </Text>
        ))
        var Deaths = data.map(item => (
            <Text key={item.Deaths}>
                {(item.Deaths)}
            </Text>
        ))
    }
    const [currentDate, setCurrentDate] = useState('');
    useEffect(() => {
        var day = new Date().getDay();
        switch (day) {
            case 1:
                day = "Senin"
                break;
            case 2:
                day = "Selasa"
                break;
            case 3:
                day = "Rabu"
                break;
            case 4:
                day = "Kamis"
                break;
            case 5:
                day = "Jum'at"
                break;
            case 6:
                day = "Sabtu"
                break;
            case 0:
                day = "Minggu"
                break;
        }
        var date = new Date().getDate();
        var month = new Date().getMonth();
        switch (month) {
            case 1:
                month = "Januari"
                break;
            case 2:
                month = "Februari"
                break;
            case 3:
                month = "Maret"
                break;
            case 4:
                month = "April"
                break;
            case 5:
                month = "Mei"
                break;
            case 6:
                month = "Juni"
                break;
            case 7:
                month = "Juli"
                break;
            case 8:
                month = "Agustus"
                break;
            case 9:
                month = "September"
                break;
            case 10:
                month = "Oktober"
                break;
            case 11:
                month = "November"
                break;
            case 12:
                month = "Desember"
                break;
        }
        month = month.substring(0, 3)
        var year = new Date().getUTCFullYear();
        setCurrentDate(`${day}, ${date} ${month} ${year}`
        );
    }, []);
    {
        var data1 = data.map(item => (
            <View key={item.Confirmed}>
                {(item.Confirmed)}
            </View>
        ))
        var data2 = []
        for (var i = 0; i < data1.length; i++) {
            data2.push(data1[i].key)
        }
        var result = data2.map(function (x) {
            return parseInt(x, 10);
        });


    }

    const Shadow = ({ line }) => (
        <Path
            key={'shadow'}
            y={1}
            d={line}
            fill={'none'}
            strokeWidth={4}
            stroke={'rgba(134, 65, 244, 0.2)'}
        />
    )
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Roboto',
                    fontWeight: '700',
                    fontSize: 30,
                    top: 25,
                    left: "10%"
                }}>{Country.pop()}</Text>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Roboto',
                    fontWeight: '700',
                    fontSize: 14,
                    top: 65,
                    left: "10%",
                    color: "#E5E5E5"
                }}>{currentDate}</Text>
                <TouchableOpacity
                    onPress={() => navigation.navigate("Gejala")}
                    style={{
                        position: 'absolute',
                        height: 100,
                        width: "90%",
                        left: 39,
                        top: 100,
                        borderTopLeftRadius: 10,
                        borderBottomLeftRadius: 10,
                        backgroundColor: "#7B61FF"
                    }}>
                    <Text style={{
                        position: 'absolute',
                        // fontFamily: 'Roboto',
                        fontWeight: '700',
                        fontSize: 18,
                        lineHeight: 20,
                        top: "10%",
                        left: "30%",
                        right: "30%",
                        position: 'absolute', color: 'white'
                    }}>Gejala Covid 19</Text>
                    <Text style={{
                        position: 'absolute',
                        // fontFamily: 'Roboto',
                        fontWeight: 'normal',
                        fontSize: 12,
                        lineHeight: 18,
                        top: "35%",
                        left: "30%",
                        right: "30%",
                        position: 'absolute', color: 'white'
                    }}>Beberapa gejala yang timbul akibat terinfeksi virus corona</Text>
                    <Image style={{ top: 30, position: 'absolute', width: 50, height: 50, opacity: 0.4 }} source={require('../images/virus.png')} />
                    <Image style={{ position: 'absolute', width: 30, height: 30, opacity: 0.4 }} source={require('../images/virus.png')} />
                    <Image style={{ top: 20, left: 50, position: 'absolute', width: 30, height: 30, opacity: 0.4 }} source={require('../images/virus.png')} />
                    <AntDesign name="right" size={24} color="#FFFFFF" style={{ position: 'absolute', left: "85%", top: "35%" }} />
                </TouchableOpacity>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Roboto',
                    fontWeight: '700',
                    fontSize: 16,
                    top: 220,
                    left: "10%"
                }}>Update Terkini Covid 19</Text>
                <TouchableOpacity style={{ top: -20, left: 280, width: 80, height: 20 }} onPress={() => navigation.navigate("Detail")}>
                    <Text style={{
                        position: 'absolute',
                        // fontFamily: 'Roboto',
                        fontWeight: '700',
                        fontSize: 10,
                        lineHeight: 10,
                        top: 5,
                        left: 10,
                        color: 'blue'
                    }}>Tampilkan</Text>
                </TouchableOpacity>
            </View >
            <View style={styles.body}>
                <View style={{
                    position: 'absolute',
                    top: 5,
                    left: "10%",
                    width: 81,
                    height: 92,
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: '#E5E5E5',
                    backgroundColor: 'white'
                }}>
                    <AntDesign name="upcircleo" size={24} color="#F2994A" style={{ marginTop: 10, textAlign: 'center' }} />
                    <Text style={{
                        color: '#F2994A',
                        fontWeight: '700',
                        textAlign: 'center',
                        fontSize: 16,
                    }}>
                        {datas.pop()}
                    </Text>
                    <Text style={{
                        color: '#BDBDBD',
                        fontWeight: '700',
                        fontSize: 12,
                        textAlign: 'center',
                    }}>Terinfeksi</Text>
                </View>
                <View style={{
                    position: 'absolute',
                    top: 5,
                    left: "40%",
                    width: 81,
                    height: 92,
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: '#E5E5E5',
                    backgroundColor: 'white'
                }}>
                    <AntDesign name="hearto" size={24} color="#27AE60" style={{ marginTop: 10, textAlign: 'center' }} />
                    <Text style={{
                        color: '#27AE60',
                        fontWeight: '700',
                        fontSize: 16,
                        textAlign: 'center',
                    }}>{Recovered.pop()}</Text>
                    <Text style={{
                        color: '#BDBDBD',
                        fontWeight: '700',
                        fontSize: 12,
                        textAlign: 'center',
                    }}>Sembuh</Text>
                </View>
                <View style={{
                    position: 'absolute',
                    top: 5,
                    left: "70%",
                    width: 81,
                    height: 92,
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: '#E5E5E5',
                    backgroundColor: 'white'
                }}>
                    <AntDesign name="closecircleo" size={24} color="#EB5757" style={{ marginTop: 10, textAlign: 'center' }} />
                    <Text style={{
                        color: '#EB5757',
                        fontWeight: '700',
                        fontSize: 16,
                        textAlign: 'center',
                    }}>{Deaths.pop()}</Text>
                    <Text style={{
                        color: '#BDBDBD',
                        fontWeight: '700',
                        fontSize: 12,
                        textAlign: 'center',
                    }}>Meninggal</Text>
                </View>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Roboto',
                    fontWeight: '700',
                    fontSize: 16,
                    top: 110,
                    left: "10%"
                }}>Grafik Covid 19</Text>
                <LineChart
                    style={{ height: 200, top: -300, left: "10%", right: "10%", width: "80%" }}
                    data={result}
                    svg={{ stroke: 'rgb(134, 65, 244)' }}
                    contentInset={{ top: 10 }}
                >
                    <Grid />
                    <Shadow />
                </LineChart>
            </View>
            <View style={styles.footer}>
                <TouchableOpacity style={{ width: 40, height: 40, left: 60, top: -30 }} onPress={() => navigation.navigate("Main")}>
                    <AntDesign name="home" size={30} color="#BDBDBD" style={{ top: 0, position: 'absolute', left: 0 }} />
                </TouchableOpacity>

                <TouchableOpacity style={{ left: 220, width: 40, height: 40, top: -65 }} onPress={() => navigation.navigate("Login")}>
                    <AntDesign name="logout" size={25} color="#BDBDBD" style={{ top: 0, position: 'absolute', left: 0 }} />
                </TouchableOpacity>
            </View>
        </View >
    )
}

export default mainPage

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        backgroundColor: 'white',
        width: 400,
        height: 800
    },
    header: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        paddingTop: "60%",
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        paddingTop: "115%",
    },
    footer: {
        flex: 1,
        backgroundColor: 'white',
        borderTopWidth: 1,
        padding: "10%",
        height: 15,
        marginBottom: 30,
        borderColor: "#BDBDBD"
    }
})
