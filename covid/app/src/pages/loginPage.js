import React from 'react'
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AntDesign } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

const loginPage = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={{
                    position: 'absolute',
                    fontSize: 80,
                    top: 0,
                    left: "35%",
                    right: "35%",
                    marginTop: 10,
                    color: 'white',
                }}>CO</Text>
                <Text style={{
                    position: 'absolute',
                    fontSize: 80,
                    top: 70,
                    left: "32.5%",
                    right: "32.5%",
                    marginTop: 10,
                    color: 'white',
                }}>-19</Text>
            </View >
            <View style={styles.body}>
                <AntDesign name="user" size={35} color="#7B61FF" style={{ position: 'absolute', left: 55, top: 80 }} />
                <AntDesign name="lock" size={35} color="#7B61FF" style={{ position: 'absolute', left: 55, top: 152 }} />
                <Text style={{
                    position: 'absolute',
                    fontSize: 12,
                    top: 50,
                    left: 90,
                    marginTop: 10,
                    color: '#7B61FF',
                }}>Username</Text>
                <TextInput style={{
                    position: 'absolute',
                    height: 40,
                    width: 240,
                    top: 80,
                    left: 90,
                    padding: 10,
                    borderBottomWidth: 2,
                    borderBottomColor: '#7B61FF'
                }} />
                <Text style={{
                    position: 'absolute',
                    fontSize: 12,
                    top: 120,
                    left: 90,
                    marginTop: 10,
                    color: '#7B61FF',
                }}>Password</Text>
                <TextInput style={{
                    position: 'absolute',
                    height: 40,
                    width: 240,
                    top: 150,
                    left: 90,
                    padding: 10,
                    borderBottomWidth: 2,
                    borderBottomColor: '#7B61FF'
                }} />

            </View>
            <View style={styles.footer}>
                <TouchableOpacity style={{ height: 20, width: 30, backgroundColor: 'blue', }} onPress={() => navigation.navigate("Main")}
                    style={{
                        position: "absolute",
                        backgroundColor: '#7B61FF',
                        top: 50,
                        left: "15%",
                        right: "15%",
                        height: 45,
                        width: 280,
                        borderRadius: 10
                    }}>

                    <Text style={{
                        position: 'absolute',
                        color: 'white',
                        fontSize: 18,
                        left: "35%",
                        right: "35%",
                        top: 10,
                        textAlign: 'center',
                    }}>SIGN IN</Text>
                </TouchableOpacity >
                <TouchableOpacity style={{
                    width: 100, height: 20, position: "absolute",
                    left: 150, top: 130
                }} onPress={() => navigation.navigate("About")}>
                    <Text style={{
                        position: "absolute",
                        top: 0,
                        left: 10,
                        color: '#7B61FF',
                        fontSize: 12,
                    }}>Tentang Kami</Text>
                </TouchableOpacity>
            </View>
        </View >
    )
}

export default loginPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: 'white',
        width: 400,
        height: 800
    },
    header: {
        flex: 1,
        backgroundColor: '#7B61FF',
        // marginTop: 10,
        marginBottom: 10,
        borderBottomLeftRadius: 130,
        textAlign: 'center',
        paddingTop: "60%",
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        paddingTop: "60%",
    },
    footer: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        paddingTop: "60%",
    }
})
