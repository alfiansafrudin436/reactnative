import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'

const splash = ({ navigation }) => {
    const Time = new Date();
    var jam = Time.getHours();
    if (jam < 12) {
        jam = "Morning"
    } else if (jam > 12 && jam < 18) {
        jam = "Evening"
    } else {
        jam = "Night"
    }
    useEffect(() => {
        setTimeout(() => {
            navigation.replace("Login")
        }, 3000)
    }, [])
    return (
        <View style={{
            flex: 1, backgroundColor: '#7B61FF', width: 390,
            height: 810,
        }}>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ position: 'absolute', top: 230, fontSize: 50, color: 'white', fontWeight: '500' }}>Good</Text>
                <Text style={{ position: 'absolute', top: 280, fontSize: 50, color: 'white', fontWeight: '500' }}>{jam}</Text>
            </View>
        </View>
    )
}

export default splash

const styles = StyleSheet.create({})
