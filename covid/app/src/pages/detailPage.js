import React, { useState, useEffect, Component } from 'react'
import {
    FlatList,
    StyleSheet, View, Text, TextInput, Button, TouchableOpacity, Image, ScrollView, SafeAreaView
} from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import axios from 'axios';


const detailPage = ({ navigation }) => {
    const [data, setData] = useState([]);

    useEffect(() => {
        axios
            .get("https://api.covid19api.com/country/indonesia")
            .then(result => setData(result.data));
    }, []);

    {
        var Country = data.map(item => (
            <Text key={item.Country}>
                {(item.Country)}
            </Text>
        ))
    }
    const [currentDate, setCurrentDate] = useState('');
    useEffect(() => {
        var day = new Date().getDay();
        switch (day) {
            case 1:
                day = "Senin"
                break;
            case 2:
                day = "Selasa"
                break;
            case 3:
                day = "Rabu"
                break;
            case 4:
                day = "Kamis"
                break;
            case 5:
                day = "Jum'at"
                break;
            case 6:
                day = "Sabtu"
                break;
            case 0:
                day = "Minggu"
                break;
        }
        var date = new Date().getDate();
        var month = new Date().getMonth();
        switch (month) {
            case 1:
                month = "Januari"
                break;
            case 2:
                month = "Februari"
                break;
            case 3:
                month = "Maret"
                break;
            case 4:
                month = "April"
                break;
            case 5:
                month = "Mei"
                break;
            case 6:
                month = "Juni"
                break;
            case 7:
                month = "Juli"
                break;
            case 8:
                month = "Agustus"
                break;
            case 9:
                month = "September"
                break;
            case 10:
                month = "Oktober"
                break;
            case 11:
                month = "November"
                break;
            case 12:
                month = "Desember"
                break;
        }
        month = month.substring(0, 3)
        var year = new Date().getUTCFullYear();
        setCurrentDate(`${day}, ${date} ${month} ${year}`
        );
    }, []);
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Roboto',
                    fontWeight: '700',
                    fontSize: 30,
                    top: 25,
                    left: "10%"
                }}>{Country.pop()}</Text>
                <Text style={{
                    position: 'absolute',
                    // fontFamily: 'Roboto',
                    fontWeight: '700',
                    fontSize: 14,
                    top: 65,
                    left: "10%",
                    color: "#E5E5E5"
                }}>{currentDate}</Text>
                <TouchableOpacity style={{
                    position: 'absolute',
                    height: 100,
                    width: "90%",
                    left: 39,
                    top: 100,
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                    backgroundColor: "#7B61FF"
                }}>
                    <Text style={{
                        position: 'absolute',
                        // fontFamily: 'Roboto',
                        fontWeight: '700',
                        fontSize: 18,
                        lineHeight: 20,
                        top: "30%",
                        left: "30%",
                        right: "30%",
                        position: 'absolute', color: 'white'
                    }}>Update Terkini Covid 19</Text>
                    <Image style={{ top: 30, position: 'absolute', width: 50, height: 50, opacity: 0.4 }} source={require('../images/virus.png')} />
                    <Image style={{ position: 'absolute', width: 30, height: 30, opacity: 0.4 }} source={require('../images/virus.png')} />
                    <Image style={{ top: 20, left: 50, position: 'absolute', width: 30, height: 30, opacity: 0.4 }} source={require('../images/virus.png')} />
                </TouchableOpacity>
            </View >
            <View style={styles.body}>
                <View style={{ borderWidth: 2, borderColor: "#7B61FF", borderRadius: 10, padding: 10, position: 'absolute', top: -50, height: 400, left: "10%", width: "80%" }}>
                    <Text style={{ marginBottom: 10, color: "#7B61FF", fontWeight: 'bold' }}>{`Tanggal-Terinfeksi-Sembuh-Meninggal`}</Text>
                    <FlatList
                        data={data}
                        keyExtractor={item => item.Date}
                        renderItem={({ item }) => (
                            <Text > { `${item.Date.slice(0, 10)}\t${item.Confirmed}\t${item.Recovered}\t${item.Deaths}`}</Text>
                        )}
                        ItemSeparatorComponent={() => <View style={{ heigh: 0.5, backgroundColor: '#E5E5E5' }} />}
                    />

                </View>
            </View>
            <View style={styles.footer}>
                <TouchableOpacity style={{ left: 145, width: 40, height: 40, top: -30 }} onPress={() => navigation.navigate("Main")}>
                    <AntDesign name="home" size={30} color="#BDBDBD" style={{ top: 0, position: 'absolute', left: 0 }} />
                </TouchableOpacity>
            </View>
        </View >
    )
}

export default detailPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: 'white',
        width: 400,
        height: 800
    },
    header: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        paddingTop: "60%",
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        paddingTop: "115%",
    },
    footer: {
        flex: 1,
        backgroundColor: 'white',
        borderTopWidth: 1,
        padding: "10%",
        height: 15,
        marginBottom: 30,
        borderColor: "#BDBDBD"
    }
})
