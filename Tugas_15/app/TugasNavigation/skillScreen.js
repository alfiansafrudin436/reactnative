import React from 'react';
import { View, StyleSheet, Image, Text, ScrollView, FlatList, SafeAreaView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import data from './skillData.json'
import SkillList from './skillList'
export default class skillScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.logo} source={require('../assets/images/logo.png')} />
                    <Icon name="user" style={styles.icon} size={22} />
                    <Text style={styles.hai}>Hai</Text>
                    <Text style={styles.textNama}>Alfian Safrudin</Text>
                    <Text style={styles.textSkills}>SKILL</Text>
                    <View style={styles.garis}>
                    </View>
                    <View style={styles.library}>
                        <Text style={styles.textFw}>Framework</Text>
                    </View>
                    <View style={styles.bahasa}>
                        <Text style={styles.textBp}>Programming</Text>
                    </View>
                    <View style={styles.teknologi}>
                        <Text style={styles.textTek}>Technologi</Text>
                    </View>
                </View>

                <View style={styles.body}>
                    <SafeAreaView style={{ flex: 1 }}>
                        <FlatList
                            data={data.items}
                            renderItem={(skill) => <SkillList skill={skill.item} />}
                            keyExtractor={(item) => item.id.toString()}
                            ItemSeparatorComponent={() => <View style={{ heigh: 0.5, backgroundColor: '#E5E5E5' }} />}
                        />
                    </SafeAreaView>
                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    body: {
        flex: 1,
        flexDirection: 'row'
    },
    header: {
        backgroundColor: 'white',
        width: 400,
        height: 200,
        elevation: 15,
    },
    logo: {
        position: 'absolute',
        width: 210,
        height: 59,
        left: 187,
        top: 8
    },
    icon: {
        position: 'absolute',
        height: 26,
        width: 26,
        left: 25,
        top: 60,
        borderRadius: 0,
        color: '#3ec6ff'

    },
    hai: {
        position: 'absolute',
        width: 21,
        height: 14,
        left: 56,
        top: 54,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 14,
        /* identical to box height */
        color: '#000000'
    },
    textNama: {
        position: 'absolute',
        width: 107,
        height: 19,
        left: 56,
        top: 68,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        /* identical to box height */
        color: '#003366'
    },
    textSkills: {
        position: 'absolute',
        width: 153,
        height: 42,
        left: 16,
        top: 103,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 36,
        lineHeight: 42,
        /* identical to box height */
        color: '#003366'
    },
    textFw: {
        position: 'absolute',
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 25,
        left: 20,
        /* identical to box height */
        color: '#003366'
    },
    textBp: {
        position: 'absolute',
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 25,
        left: 20,
        /* identical to box height */
        color: '#003366'
    },
    textTek: {
        position: 'absolute',
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 25,
        left: 6,
        /* identical to box height */
        color: '#003366'
    },
    garis: {
        position: 'absolute',
        width: 365,
        height: 4,
        left: 16,
        top: 145,
        backgroundColor: '#3EC6FF'
    },
    library: {
        position: 'absolute',
        width: 125,
        height: 32,
        left: 16,
        top: 159,
        borderRadius: 8,
        backgroundColor: '#3EC6FF'
    },
    bahasa: {
        position: 'absolute',
        width: 136,
        height: 32,
        left: 147,
        top: 159,
        borderRadius: 8,
        backgroundColor: '#3EC6FF'
    },
    teknologi: {
        position: 'absolute',
        width: 90,
        height: 32,
        left: 289,
        top: 159,
        borderRadius: 8,
        backgroundColor: '#3EC6FF'
    },

})
