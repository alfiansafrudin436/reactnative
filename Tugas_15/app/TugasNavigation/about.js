import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import skillScreen from '../TugasNavigation/skillScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// const Tab = createBottomTabNavigator();

const About = () => {
  return (
    <View style={styles.container}>
      <ScrollView style={{ flex: 1 }}>
        <View style={styles.body}>
          <Text style={styles.textTentangSaya}>Tentang Saya</Text>
          <TouchableOpacity>
            <Icon name="circle" size={150} color="#EFEFEF" style={styles.iconCircle} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="user" size={90} color="#CACACA" style={styles.iconUser} />
          </TouchableOpacity>
          <Text style={styles.textNama}>Alfian Safrudin</Text>
          <Text style={styles.textJob}>React Native Developer</Text>
          <View style={styles.portofilio}></View>
          <Text style={styles.textPortofolio}>Portofolio</Text>
          <TouchableOpacity>
            <Icon name="gitlab" size={50} color="#3EC6FF" style={styles.iconGitlab} />
          </TouchableOpacity>
          <Text style={styles.textNamaGitlab}>@alfiansafrudin436</Text>
          <View style={styles.garisPortofilio}></View>

          <View style={styles.contact}></View>
          <Text style={styles.textContact}>Hubungi Saya</Text>
          <TouchableOpacity>
            <Icon name="facebook" size={50} color="#3EC6FF" style={styles.iconFacebook} />
          </TouchableOpacity>
          {/* <TouchableOpacity>
          <Icon name="instagram" size={50} color="#3EC6FF" style={styles.iconInstagram} />
        </TouchableOpacity> */}
          <View style={styles.garisContact}></View>
          <Text style={styles.textNamaFacebook}>Alfian Safrudin</Text>
          {/* <Text style={styles.textNamaInstagram}>fian_saf12</Text> */}
        </View>
      </ScrollView>
    </View>
  )
}
export default About
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    position: 'absolute',
    flex: 1,
    // backgroundColor: '#FFFFFF'
  },
  textTentangSaya: {
    position: 'absolute',
    width: 220,
    height: 42,
    left: 90,
    top: 64,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 36,
    lineHeight: 42,
    color: '#003366'
  },
  iconCircle: {
    position: 'absolute',
    height: 200,
    width: 200,
    left: 132,
    top: 130,

  },
  iconUser: {
    position: 'absolute',
    height: 136,
    width: 136,
    left: 162,
    top: 150,

  },
  iconGitlab: {
    position: 'absolute',
    height: 136,
    width: 136,
    left: 162,
    top: 400,

  },
  iconFacebook: {
    position: 'absolute',
    height: 136,
    width: 136,
    left: 182,
    top: 580,

  },
  iconInstagram: {
    position: 'absolute',
    height: 136,
    width: 136,
    left: 178,
    top: 660,

  },
  textNama: {
    position: 'absolute',
    width: 163,
    height: 28,
    left: 118,
    top: 290,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366'
  },
  textJob: {
    position: 'absolute',
    width: 168,
    height: 19,
    left: 113,
    top: 320,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    color: '#3EC6FF'
  },
  portofilio: {
    position: 'absolute',
    height: 150,
    width: 375,
    left: 8,
    top: 345,
    backgroundColor: '#EFEFEF',
    borderRadius: 25
  },
  contact: {
    position: 'absolute',
    height: 250,
    width: 375,
    left: 8,
    top: 510,
    backgroundColor: '#EFEFEF',
    borderRadius: 25
  },
  garisPortofilio: {
    position: 'absolute',
    height: 0,
    width: 345,
    left: 20,
    top: 385,
    backgroundColor: '#003366',
    borderWidth: 1
  },
  garisContact: {
    position: 'absolute',
    height: 0,
    width: 345,
    left: 20,
    top: 550,
    backgroundColor: '#003366',
    borderWidth: 1
  },
  textPortofolio: {
    position: 'absolute',
    width: 80,
    height: 21,
    left: 21,
    top: 355,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 21,
    color: '#003366'
  },
  textNamaGitlab: {
    position: 'absolute',
    width: 150,
    height: 21,
    left: 120,
    top: 455,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 21,
    color: '#003366'
  },
  textContact: {
    position: 'absolute',
    width: 150,
    height: 21,
    left: 21,
    top: 520,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 21,
    color: '#003366'
  },
  textNamaFacebook: {
    position: 'absolute',
    width: 150,
    height: 21,
    left: 150,
    top: 635,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 21,
    color: '#003366'
  },
  textNamaInstagram: {
    position: 'absolute',
    width: 150,
    height: 21,
    left: 160,
    top: 710,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 21,
    color: '#003366'
  },
});
