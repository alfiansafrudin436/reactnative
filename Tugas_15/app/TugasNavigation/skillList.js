import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, _ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default class skillList extends Component {
    render() {
        let skill = this.props.skill;

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.skill}>
                        <Text style={styles.namaSkill}>{skill.skillName}</Text>
                        <Image source={{ uri: skill.logoUrl }} style={{ left: 30, top: 30, width: 80, height: 80, tintColor: "#3c3c3c" }} />
                        <Text style={styles.namaCategory}>{skill.category}</Text>
                        <Text style={styles.percentage}>{skill.percentageProgress}</Text>
                        <Icon name="keyboard-arrow-right" style={styles.arrowIcon} size={60} />
                    </View >
                </View >
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        paddingBottom: 150,
        flex: 1
    },
    skill: {
        flex: 1,
        position: 'absolute',
        flexDirection: 'row',
        width: 365,
        height: 130,
        marginLeft: 15,
        marginRight: 15,
        top: 20,
        borderRadius: 8,
        shadowRadius: 4,
        backgroundColor: '#3EC6FF',
    },
    namaSkill: {
        flex: 1,
        position: 'absolute',
        width: 150,
        height: 28,
        left: 105,
        textAlign: 'center',
        top: 15,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 28,
        color: '#003366'
    },
    namaCategory: {
        flex: 1,
        position: 'absolute',
        width: 160,
        height: 29,
        left: 100,
        textAlign: 'center',
        top: 45,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 16,
        color: '#003366'
    },
    percentage: {
        flex: 1,
        position: 'absolute',
        width: 100,
        height: 56,
        left: 145,
        textAlign: 'center',
        top: 60,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 40,
        lineHeight: 56,
        color: '#FFFFFF'
    },
    arrowIcon: {
        flex: 1,
        position: 'absolute',
        top: 35,
        left: 280,
        color: '#003366'
    }
});
