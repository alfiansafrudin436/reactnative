import React from 'react'
import { StyleSheet } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Login from '../TugasNavigation/login'
import about from '../TugasNavigation/about'
import skillScreen from '../TugasNavigation/skillScreen'
import AddProject from '../TugasNavigation/AddProject'
import Project from '../TugasNavigation/ProjectScreen'
import Home from './Home'
import About from '../TugasNavigation/about';

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const AddProjectStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const SkillStack = createStackNavigator();


const AddProjectStackScreen = () => {
    return (
        <AddProjectStack.Navigator >
            <AddProjectStack.Screen name="Add Project" component={AddProject} />
        </AddProjectStack.Navigator >
    )
}
const ProjectStackScreen = () => {
    return (
        <ProjectStack.Navigator>
            <ProjectStack.Screen name="Project " component={Project} />
        </ProjectStack.Navigator>
    )
}
const SkillStackScreen = () => {
    return (
        <SkillStack.Navigator>
            <SkillStack.Screen name="Skill Screen" component={skillScreen} />
        </SkillStack.Navigator>
    )
}

const AboutStack = createStackNavigator();


const AboutStackScreen = () => {
    return (
        <AboutStack.Navigator>
            <AboutStack.Screen name="About" component={about} />
        </AboutStack.Navigator>
    )
}
const TabScreen = () => {
    return (
        <Tab.Navigator >
            <Tab.Screen name="Skill Screen" component={SkillStackScreen} />
            <Tab.Screen name="Project Screen" component={ProjectStackScreen} />
            <Tab.Screen name="Add Screen" component={AddProjectStackScreen} />
        </Tab.Navigator >
    )
}

const index = () => {
    return (
        <NavigationContainer>
            <Drawer.Navigator>
                <Drawer.Screen name="Home" component={TabScreen} />
                <Drawer.Screen name="About" component={AboutStackScreen} />
            </Drawer.Navigator>
            {/* <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} />
            </Stack.Navigator> */}
        </NavigationContainer>
    )
}


export default index


const styles = StyleSheet.create({

})