import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, FlatList, ScrollView, Image } from 'react-native'
import IconF from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Product from '../src/list'
import data from '../src/dataStore.json'
export default class halamanMenu extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={{
                        height: 44,
                        width: 352,
                        left: 16,
                        top: 56,
                        borderRadius: 11,
                        borderWidth: 3,
                        borderColor: '#E6EAEE',

                    }}>
                        <Icon name="search" size={25} style={{
                            color: '#727C8E',
                            marginTop: 6,
                            marginLeft: 15

                        }} />
                        <Icon name="camera" size={25} style={{
                            color: '#727C8E',
                            marginTop: -25,
                            marginLeft: 300

                        }} />
                        <Icon name="notifications" size={25} style={{
                            color: '#727C8E',
                            marginTop: -25,
                            marginLeft: 370

                        }} />
                    </View>
                </View>

                <View style={styles.body}>
                    <Image source={require("./img/shop.jpg")} style={{ left: 18, top: 1, width: 385, height: 120, borderRadius: 10 }}></Image>
                    <IconF name="male" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: 20,
                            marginLeft: 40,
                            color: '#FF7171'

                        }} />
                    <IconF name="female" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: -30,
                            marginLeft: 133,
                            color: "#A2F0E6"

                        }} />
                    <IconF name="male" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: -30,
                            marginLeft: 239,
                            color: "#7AD0FF"

                        }} />
                    <IconF name="home" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: -30,
                            marginLeft: 320,
                            color: "#CCAAFF",



                        }} />
                    <ScrollView horizontal>
                        <FlatList style={{ width: 385, left: 18, top: 20 }}
                            data={data.items}
                            renderItem={(product) => <Product product={product.item} />}
                            keyExtractor={(item) => item.id.toString()}
                        // ItemSeparatorComponent={() => <View style={{ heigh: 0.5, backgroundColor: '#E5E5E5' }} />}
                        />
                    </ScrollView>
                </View >

                <View style={styles.footer}>
                    <Icon name="home" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: 20,
                            marginLeft: 40

                        }} />
                    <IconF name="shopping-cart" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: -30,
                            marginLeft: 133

                        }} />
                    <Icon name="mail" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: -30,
                            marginLeft: 239

                        }} />
                    <Icon name="person" size={30}
                        style={{
                            color: '#727C8E',
                            marginTop: -30,
                            marginLeft: 320

                        }} />
                </View>
            </View >
        )
    }
}
const styles = StyleSheet.create({
    header: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        height: 120,
        width: 414,
        left: 0,
        top: 0
    },
    body: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        height: 345,
        width: 414,
        left: 0,
        top: 130
    },
    footer: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        shadowColor: '#3c3c3c',
        shadowOpacity: 10,
        shadowRadius: 3,
        borderColor: '#E6EAEE',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        height: 80,
        width: 414,
        left: 0,
        top: 500
    },
})