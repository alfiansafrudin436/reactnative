import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity, _ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default class List extends Component {
    render() {
        let product = this.props.product;

        return (
            <View style={styles.container}>
                <Text style={styles.namaSkill}>{product.productName}</Text>
                <Image source={{ uri: product.pictureUrl }} style={{ width: 100, height: 100 }} />
                <Text style={styles.namaCategory}>{product.category}</Text>
                <Text style={styles.percentage}>{product.price}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        paddingTop: 5,
        flex: 1
    },
    store: {
        flex: 1,
        flexDirection: 'row',
        width: 365,
        height: 129,
        marginLeft: 15,
        marginRight: 15,
        top: 2,
        borderRadius: 8,
        shadowRadius: 4,
        backgroundColor: '#3EC6FF'
    },
    namaSkill: {
        flex: 1,
        position: 'absolute',
        width: 150,
        height: 28,
        left: 105,
        textAlign: 'center',
        top: 15,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 28,
        color: '#003366'
    },
    namaCategory: {
        flex: 1,
        position: 'absolute',
        width: 160,
        height: 29,
        left: 100,
        textAlign: 'center',
        top: 45,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 16,
        color: '#003366'
    },
    percentage: {
        flex: 1,
        position: 'absolute',
        width: 100,
        height: 56,
        left: 145,
        textAlign: 'center',
        top: 60,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 40,
        lineHeight: 56,
        color: '#FFFFFF'
    },
    arrowIcon: {
        flex: 1,
        position: 'absolute',
        top: 35,
        left: 280,
        color: '#003366'
    }
});
