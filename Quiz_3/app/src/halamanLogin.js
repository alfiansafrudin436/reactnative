import React from 'react'
import { Button, StyleSheet, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class HalamanLogin extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text style={styles.welcome}>Welocome</Text>
                <Text style={styles.welcome2}>Sign in to continue</Text>
                <View style={styles.body}>
                    <Text style={styles.text1}>Name</Text>
                    <TextInput style={styles.textInputName} />
                    <Text style={styles.text2}>Password</Text>
                    <TextInput style={styles.textInputPass} />
                    <Text style={styles.text3}>Forgot Password ?</Text>


                    <View style={{ flex: 1 }}>
                        <View style={{
                            height: 50,
                            width: 300,
                            left: 32,
                            top: 230,
                            borderRadius: 6,
                            backgroundColor: '#F77866'
                        }}></View>
                        <View style={{
                            height: 40,
                            width: 130,
                            left: 32,
                            top: 290,
                            borderRadius: 6,
                            backgroundColor: '#FFFFFF',
                            borderColor: '#E6EAEE',
                            borderWidth: 3
                        }}>
                            <Icon name="facebook" size={20} style={{ marginLeft: 20, marginTop: 5 }} />
                            <Text style={styles.text6}>Facebook</Text>
                        </View>

                        <View style={{
                            height: 40,
                            width: 130,
                            left: 200,
                            top: 250,
                            borderRadius: 6,
                            backgroundColor: '#FFFFFF',
                            borderColor: '#E6EAEE',
                            borderWidth: 3
                        }}>
                            <Icon name="google" size={20} style={{ marginLeft: 20, marginTop: 5 }} />
                            <Text style={styles.text5}>Google</Text>
                        </View>
                        < Text style={styles.signUp} > SIGN IN</Text>
                        < Text style={styles.text4} >-OR-</Text>
                    </View>

                </View >

            </View >
        )
    }

}

const styles = StyleSheet.create({
    welcome: {
        position: 'absolute',
        height: 37,
        width: 149,
        top: 100,
        left: 20,
        fontFamily: 'Montserrat',
        fontSize: 30,
        fontStyle: 'normal',
        fontWeight: 700,
        lineHeight: 37,
        letterspacing: 0,
        textalign: 'center'
    },
    welcome2: {
        position: 'absolute',
        height: 37,
        width: 149,
        top: 130,
        left: 30,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 700,
        lineHeight: 37,
        letterspacing: 0,
        textalign: 'center'
    },
    body: {
        height: 536,
        width: 366,
        left: 32,
        top: 249,
        borderRadius: 11,
        backgroundColor: '#FFFFF',
        shadowRadius: 8,
        shadowColor: '#3c3c3c'



    },
    text1: {
        position: 'absolute',
        height: 15,
        width: 37,
        top: 50,
        left: 32,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text2: {
        position: 'absolute',
        height: 15,
        width: 100,
        top: 110,
        left: 32,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    textInputName: {
        position: 'absolute',
        width: 300,
        height: 40,
        top: 52,
        left: 32,
        paddingLeft: 15,
        borderBottomColor: '#E6EAEE',
        borderBottomWidth: 2,

    },
    textInputPass: {
        position: 'absolute',
        width: 300,
        height: 40,
        top: 110,
        left: 32,
        paddingLeft: 15,
        borderBottomColor: '#E6EAEE',
        borderBottomWidth: 2,

    },
    signUp: {
        position: 'absolute',
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontStyle: 'normal',
        fontWeight: 600,
        lineHeight: 17,
        color: '#ffffff',
        width: 318,
        height: 40,
        top: 245,
        left: 150
    },
    text3: {
        position: 'absolute',
        height: 15,
        width: 370,
        top: 170,
        left: 240,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text4: {
        position: 'absolute',
        height: 15,
        width: 370,
        top: 300,
        left: 170,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text5: {
        position: 'absolute',
        height: 15,
        width: 370,
        top: 10,
        left: 50,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '600',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text6: {
        position: 'absolute',
        height: 15,
        width: 370,
        top: 10,
        left: 50,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '600',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },

});
