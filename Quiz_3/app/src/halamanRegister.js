import React from 'react'
import { Button, StyleSheet, Text, TextInput, View } from 'react-native'

export default class HalamanRegister extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text style={styles.welcome}>Welocome</Text>
                <Text style={styles.welcome2}>Sign up to continue</Text>
                <View style={styles.body}>
                    <Text style={styles.text1}>Name</Text>
                    <TextInput style={styles.textInputName} />
                    <Text style={styles.text2}>Email</Text>
                    <TextInput style={styles.textInputEmail} />
                    <Text style={styles.text3}>Phone Number</Text>
                    <TextInput style={styles.textInputPhone} />
                    <Text style={styles.text4}>Password</Text>
                    <TextInput style={styles.textInputPassword} />
                </View >
                <View style={{ flex: 1 }}>
                    <View style={{
                        height: 50,
                        width: 300,
                        left: 32,
                        top: 230,
                        borderRadius: 6,
                        backgroundColor: '#F77866'
                    }}></View>
                    < Text style={styles.signUp} > SIGN UP</Text>
                    < Text style={styles.text5} > Already have an account ?</Text>
                    < Text style={styles.text6} > SIGN IN</Text>
                </View>

            </View >
        )
    }

}

const styles = StyleSheet.create({
    welcome: {
        position: 'absolute',
        height: 37,
        width: 149,
        top: 100,
        left: 20,
        fontFamily: 'Montserrat',
        fontSize: 30,
        fontStyle: 'normal',
        fontWeight: 700,
        lineHeight: 37,
        letterspacing: 0,
        textalign: 'center'
    },
    welcome2: {
        position: 'absolute',
        height: 37,
        width: 149,
        top: 130,
        left: 30,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 700,
        lineHeight: 37,
        letterspacing: 0,
        textalign: 'center'
    },
    body: {
        height: 536,
        width: 366,
        left: 32,
        top: 249,
        borderRadius: 11,
        backgroundColor: '#FFFFF',
        shadowRadius: 8,
        shadowColor: '#3c3c3c'



    },
    text1: {
        position: 'absolute',
        height: 15,
        width: 37,
        top: 20,
        left: 32,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text2: {
        position: 'absolute',
        height: 15,
        width: 37,
        top: 80,
        left: 32,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text3: {
        position: 'absolute',
        height: 15,
        width: 80,
        top: 135,
        left: 32,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text4: {
        position: 'absolute',
        height: 15,
        width: 80,
        top: 190,
        left: 32,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    textInputName: {
        position: 'absolute',
        width: 300,
        height: 40,
        top: 22,
        left: 32,
        paddingLeft: 15,
        borderBottomColor: '#E6EAEE',
        borderBottomWidth: 2,

    },
    textInputEmail: {
        position: 'absolute',
        width: 300,
        height: 40,
        top: 80,
        left: 32,
        paddingLeft: 15,
        borderBottomColor: '#E6EAEE',
        borderBottomWidth: 2,

    },
    textInputPhone: {
        position: 'absolute',
        width: 300,
        height: 40,
        top: 135,
        left: 32,
        paddingLeft: 15,
        borderBottomColor: '#E6EAEE',
        borderBottomWidth: 2,

    },
    textInputPassword: {
        position: 'absolute',
        width: 300,
        height: 40,
        top: 190,
        left: 32,
        paddingLeft: 15,
        borderBottomColor: '#E6EAEE',
        borderBottomWidth: 2,

    },
    signUp: {
        position: 'absolute',
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontStyle: 'normal',
        fontWeight: 600,
        lineHeight: 17,
        color: '#ffffff',
        width: 318,
        height: 40,
        top: 98,
        left: 180
    },
    text5: {
        position: 'absolute',
        height: 15,
        width: 370,
        top: 140,
        left: 130,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center'
    },
    text6: {
        position: 'absolute',
        height: 15,
        width: 370,
        top: 140,
        left: 260,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 15,
        letterspacing: 0,
        textalign: 'center',
        color: '#F77866',
    },
});
