import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default class HalamanAwal extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.circle}>
                </View>
                <Image style={styles.image} source={require('./img/logo@2x.jpg')} />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    circle: {
        position: 'absolute',
        height: 305,
        width: 298,
        left: 57,
        top: 279,
        bordeRadius: 0,
        background: '#211F65',
        opacity: 0.1
    },
    image: {
        position: 'absolute',
        width: 223,
        height: 133,
        left: 101,
        top: 365
    }
})
