import React from 'react'
import { Text, View } from 'react-native';
import FirstPage from './src/halamanAwal';
import RegisterPage from './src/halamanRegister';
import LoginPage from './src/halamanLogin';
import MainPage from './src/halamanMenu';

const App = () => {
  return (
    <View>

      {/* <FirstPage /> */}
      {/* <RegisterPage /> */}
      {/* <LoginPage /> */}
      <MainPage />
    </View>
  )

}
export default App