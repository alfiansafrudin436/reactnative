import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, Button, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
export default function App() {
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <Image source={require('./assets/images/logo.png')} style={styles.image}></Image>
                <Text style={styles.text}>Register</Text>
                <Text style={styles.textUsername}>Username</Text>
                <Text style={styles.textEmail}>Email</Text>
                <Text style={styles.textPassword}>Password</Text>
                <Text style={styles.textRePassword}>Ulangi Password</Text>

                <TextInput style={styles.textInputUsername} />
                <TextInput style={styles.textInputEmail} />
                <TextInput style={styles.textInputPassword} />
                <TextInput style={styles.textInputRePassword} />

                <TouchableOpacity style={styles.buttonDaftar}>
                    <Text style={{
                        position: 'absolute',
                        width: 90,
                        height: 28,
                        left: 35,
                        bottom: 8,
                        fontFamily: 'Roboto',
                        fontStyle: 'normal',
                        fontWeight: 'normal',
                        fontSize: 24,
                        lineHeight: 28,
                        color: '#ffffff',
                    }}>Daftar</Text>
                </TouchableOpacity>
                <Text style={styles.textAtau}>atau</Text>
                <TouchableOpacity style={styles.buttonMasuk}>
                    <Text style={{
                        position: 'absolute',
                        width: 90,
                        height: 28,
                        left: 35,
                        top: 4,
                        fontFamily: 'Roboto',
                        fontStyle: 'normal',
                        fontWeight: 'normal',
                        fontSize: 24,
                        lineHeight: 28,
                        color: '#ffffff',
                    }}>Masuk</Text>
                </TouchableOpacity>

            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    image: {
        position: 'absolute',
        width: 375,
        height: 102,
        left: 0,
        top: 63
    },
    text: {
        position: 'absolute',
        width: 88,
        height: 28,
        left: 143,
        top: 235,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        color: '#003366'
    },
    textUsername: {
        position: 'absolute',
        width: 73,
        height: 19,
        left: 40,
        top: 303,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        /* identical to box height */
        color: '#003366'
    },
    textEmail: {
        position: 'absolute',
        width: 73,
        height: 19,
        left: 40,
        top: 390,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        /* identical to box height */
        color: '#003366'
    },
    textPassword: {
        position: 'absolute',
        width: 73,
        height: 19,
        left: 40,
        top: 477,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        /* identical to box height */
        color: '#003366'
    },
    textRePassword: {
        position: 'absolute',
        width: 200,
        height: 19,
        left: 40,
        top: 564,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        /* identical to box height */
        color: '#003366'
    },
    textInputUsername: {
        position: 'absolute',
        width: 310,
        height: 48,
        left: 41,
        top: 326,
        paddingLeft: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#003366',
        borderRadius: 10,
        borderTopLeftRadius: 1,
        borderWidth: 2
    },
    textInputEmail: {
        position: 'absolute',
        width: 310,
        height: 48,
        left: 41,
        top: 413,
        paddingLeft: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#003366',
        borderRadius: 10,
        borderTopLeftRadius: 1,
        borderWidth: 2
    },
    textInputPassword: {
        position: 'absolute',
        width: 310,
        height: 48,
        left: 41,
        top: 500,
        paddingLeft: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#003366',
        borderRadius: 10,
        borderTopLeftRadius: 1,
        borderWidth: 2
    },
    textInputRePassword: {
        position: 'absolute',
        width: 310,
        height: 48,
        left: 41,
        top: 587,
        paddingLeft: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#003366',
        borderRadius: 10,
        borderTopLeftRadius: 1,
        borderWidth: 2
    },
    buttonDaftar: {
        position: 'absolute',
        width: 140,
        height: 40,
        left: 117,
        top: 675,
        backgroundColor: '#003366',
        borderRadius: 10
    },
    buttonDaftar: {
        position: 'absolute',
        width: 140,
        height: 40,
        left: 117,
        top: 675,
        backgroundColor: '#003366',
        borderRadius: 10
    },
    buttonMasuk: {
        position: 'absolute',
        width: 140,
        height: 40,
        left: 117,
        top: 750,
        backgroundColor: '#3EC6FF',
        borderRadius: 10
    },
    textAtau: {
        position: 'absolute',
        width: 48,
        height: 28,
        left: 164,
        top: 716,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        /* identical to box height */
        color: '#3EC6FF'
    },
});
